---
title: "About"
date: 2024-03-15
draft: false

showDate : false
showDateUpdated : false
showHeadingAnchors : false
showPagination : false
showReadingTime : false
showTableOfContents : true
showTaxonomies : false 
showWordCount : false
showSummary : false
sharingLinks : false
showEdit: false
showViews: true
showLikes: false
showAuthor: true
layoutBackgroundHeaderSpace: false
---

My name is Tyler 👋, but I go by Zaney online. I have been using Linux for years now and really enjoy what the ecosystem has to offer. I create videos on my experience with Linux, share my configuration files, and try to share what I've learned with anyone in the community.
